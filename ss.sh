#!/bin/bash

file=$(find "/home/fesa/pic" -type f | dmenu -i -l 25)
curl -F "file=@$file" 0x0.st | xclip -selection c
notify-send "Your file has been uploaded, in ur clipboard"
